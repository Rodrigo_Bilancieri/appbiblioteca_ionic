/**
 * @author Nicholas Luis Braga Sebastião <nicholaslbsebastiao@gmail.com>
 * @author Everton Godoy Duarte <evertongodoyduarte@gmail.com>
 * @author Rodrigo Bilancieri <rodrigobilancieri@gmail.com>
 * @author Victor Adriano Antunes Zanin <victoradriano1704@gmail.com>
 * 
 */

var restify = require('restify');
var corsMiddleware = require('restify-cors-middleware');

const server = restify.createServer({
    name: 'unipbiblio',
    version: '1.0.0'
});

const cors = corsMiddleware({
    preflightMaxAge: 5, //Optional
    origins: ['http://localhost:8100'],
    allowHeaders: ["Access-Control-Allow-Origin", "Access-Control-Allow-Headers", "X-Requested-With"],
    //exposeHeaders: ['API-Token-Expiry']
})

server.pre(cors.preflight)
server.use(cors.actual)

server.use(restify.plugins.acceptParser(server.acceptable));
server.use(restify.plugins.queryParser());
server.use(restify.plugins.bodyParser());


server.get('/', function (req, res, next) {
    res.json({ mensagem: "Servidor ativado" });
    next();
})

/**
Resources
@type {AutorResource|*|exports|module.exports}
@type { LivroResource |*| exports | module.exports }
*/
var AutorResource = require('./resources/autorResource');
autorResource = new AutorResource();

var LivroResource = require('./resources/livroResource');
livroResource = new LivroResource();

var UsuarioResource = require('./resources/usuarioResource');
usuarioResource = new UsuarioResource();

var EmprestimoResource = require('./resources/emprestimoResource');
emprestimoResource = new EmprestimoResource();

var LivroEmprestimoResource = require('./resources/livroEmprestimoResource');
livroEmprestimoResource = new LivroEmprestimoResource();

var LivroAutorResource = require('./resources/livroAutorResource');
livroAutorResource = new LivroAutorResource();

/**
* GET, POST, PUT e DELETE - Resource Autor
*/
server.get('/api/v1/unip/biblioteca/autor', autorResource.findAll);
server.post('/api/v1/unip/biblioteca/autor', autorResource.post);
server.put('/api/v1/unip/biblioteca/autor', autorResource.update);
server.del('/api/v1/unip/biblioteca/autor/:auId', autorResource.delete);

/**
* GET, POST, PUT e DELETE - Resource Livro
*/
server.get('/api/v1/unip/biblioteca/livro', livroResource.findAll);
server.post('/api/v1/unip/biblioteca/livro', livroResource.post);
server.put('/api/v1/unip/biblioteca/livro', livroResource.update);
server.del('/api/v1/unip/biblioteca/livro/:lvId', livroResource.delete);

/**
* GET, POST, PUT e DELETE - Resource Emprestimo
*/
server.get('/api/v1/unip/biblioteca/buscaid/:usId', emprestimoResource.buscaId);
server.get('/api/v1/unip/biblioteca/emprestimo/:usId', emprestimoResource.findAll);
server.post('/api/v1/unip/biblioteca/emprestimo', emprestimoResource.post);
server.put('/api/v1/unip/biblioteca/emprestimo', emprestimoResource.update);
server.del('/api/v1/unip/biblioteca/emprestimo', emprestimoResource.delete);

/**
* GET, POST, PUT e DELETE - Resource LivroEmprestimo
*/
server.get('/api/v1/unip/biblioteca/livroemprestimo/:emId', livroEmprestimoResource.findAll);
server.post('/api/v1/unip/biblioteca/livroemprestimo', livroEmprestimoResource.post);
server.put('/api/v1/unip/biblioteca/livroemprestimo', livroEmprestimoResource.update);
server.del('/api/v1/unip/biblioteca/livroemprestimo', livroEmprestimoResource.delete);

/**
* GET, POST, PUT e DELETE - Resource LivroAutor
*/
server.get('/api/v1/unip/biblioteca/livroautor', livroAutorResource.findAll);
server.post('/api/v1/unip/biblioteca/livroautor', livroAutorResource.post);
server.put('/api/v1/unip/biblioteca/livroautor', livroAutorResource.update);
server.del('/api/v1/unip/biblioteca/livroautor', livroAutorResource.delete);

/**
 * GET e POST - Usuario
 */
server.get('/api/v1/unip/biblioteca/usuario/:usEmail', usuarioResource.findAll);
server.post('/api/v1/unip/biblioteca/usuario', usuarioResource.post);

// Variável serve para identificar a porta correta de acordo com o ambiente de execução.
// Se não estiver em localhost, pega a porta que o ambiente disponibilizar, senão, usa a porta 3000
port = process.env.PORT || 3000

server.listen(port, function () {
    console.log('%s ativado no endereço http://localhost:3000', server.name);
});