"use strict";
class LivroResource {

    findAll(req, res, next) {
        const LivroBusiness = require("../business/livroBusiness");
        this.livroBusiness = new LivroBusiness();
        this.livroBusiness.findAll(
            function callbackLivro(rows) {
                res.json(rows); next();
            }
        );
    }

    post(req, res, next) {
        const LivroBusiness = require("../business/livroBusiness");
        this.livroBusiness = new LivroBusiness();
        this.livroBusiness.post(req.body,
            function callbackLivro(rows) {
                res.json(rows); next();
            }
        );
    }

    update(req, res, next) {
        const LivroBusiness = require("../business/livroBusiness");
        this.livroBusiness = new LivroBusiness();
        this.livroBusiness.update(req.body,
            function callbackLivro(rows) {
                res.json(rows); next();
            }
        );
    }

    delete(req, res, next) {
        const LivroBusiness = require("../business/livroBusiness");
        this.livroBusiness = new LivroBusiness();
        this.livroBusiness.delete(req.params,
            function callbackLivro(rows) {
                res.json(rows); next();
            }
        );
    }
}
module.exports = LivroResource;
