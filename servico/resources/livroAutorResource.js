"use strict";
class LivroAutorResource {

    findAll(req, res, next) {
        const LivroAutorBusiness = require("../business/livroAutorBusiness");
        this.livroAutorBusiness = new LivroAutorBusiness();
        this.livroAutorBusiness.findAll(
            function callbackLivroAutor(rows) {
                res.json(rows); next();
            }
        );
    }

    post(req, res, next) {
        const LivroAutorBusiness = require("../business/livroAutorBusiness");
        this.livroAutorBusiness = new LivroAutorBusiness();
        this.livroAutorBusiness.post(req.body,
            function callbackLivroAutor(rows) {
                res.json(rows); next();
            }
        );
    }

    update(req, res, next) {
        const LivroAutorBusiness = require("../business/livroAutorBusiness");
        this.livroAutorBusiness = new LivroAutorBusiness();
        this.livroAutorBusiness.update(req.body,
            function callbackLivroAutor(rows) {
                res.json(rows); next();
            }
        );
    }

    delete(req, res, next) {
        const LivroAutorBusiness = require("../business/livroAutorBusiness");
        this.livroAutorBusiness = new LivroAutorBusiness();
        this.livroAutorBusiness.delete(req.body,
            function callbackLivroAutor(rows) {
                res.json(rows); next();
            }
        );
    }
}
module.exports = LivroAutorResource;
