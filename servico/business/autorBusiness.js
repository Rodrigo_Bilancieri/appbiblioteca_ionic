"use strict";

class AutorBusiness {

    /*findAll(callbackAutor) {
    const SessionFactory = require("../factory/sessionFactory");
    this.session = new SessionFactory();
    this.session.connect();
    this.session.connection.query('SELECT * from Autor',
        function (err, rows, fields) {
            if (err)
                throw err;
            callbackAutor(rows);
            if (this.session)
                this.session.end();
        });
    }*/

    findAll(callbackAutor) {
        const SessionFactory = require("../factory/sessionFactory");
        this.session = new SessionFactory();

        var that = this;
        this.session.connect(function () {

            that.session.connection.query('SELECT * from Autor',
                function (err, rows, fields) {
                    if (err)
                        throw err;
                    callbackAutor(rows);
                    if (that.session)
                        that.session.end();
                });
        });
    }

    post(autor, callbackAutor) {
        const SessionFactory = require("../factory/sessionFactory");
        this.session = new SessionFactory();

        var that = this;
        this.session.connect(function () {

            that.session.connection.query("INSERT INTO Autor (auNome, auIdade, auFoto) values ('" + autor.auNome + "', " + autor.auIdade + ", '" + autor.auFoto + "')",
                function (err, rows, fields) {
                    if (err)
                        throw err;
                    callbackAutor(rows);
                    if (that.session)
                        that.session.end();
                });
        });
    }

    //"UPDATE Autor SET auNome = '" + autor.nome + "' WHERE auId = " + autor.id + ""
    update(autor, callbackAutor) {
        const SessionFactory = require("../factory/sessionFactory");
        this.session = new SessionFactory();

        var that = this;
        this.session.connect(function () {

            that.session.connection.query("UPDATE Autor SET auNome = '" + autor.auNome + "', auIdade = " + autor.auIdade + ", auFoto = '" + autor.auFoto + "' WHERE auId = " + autor.auId + "",
                function (err, rows, fields) {
                    if (err)
                        throw err;
                    callbackAutor(rows);
                    if (that.session)
                        that.session.end();
                });
        });
    }

    delete(autor, callbackAutor) {
        const SessionFactory = require("../factory/sessionFactory");
        this.session = new SessionFactory();

        var that = this;
        this.session.connect(function () {

            that.session.connection.query("DELETE FROM Autor WHERE auId = " + autor.auId + "",
                function (err, rows, fields) {
                    if (err)
                        throw err;
                    callbackAutor(rows);
                    if (that.session)
                        that.session.end();
                });
        });
    }

}
module.exports = AutorBusiness;