"use strict";

class LivroBusiness {

    findAll(callbackLivro) {
        const SessionFactory = require("../factory/sessionFactory");
        this.session = new SessionFactory();

        var that = this;
        this.session.connect(function () {

            that.session.connection.query("SELECT * FROM Livro;",
                function (err, rows, fields) {
                    if (err)
                        throw err;
                    callbackLivro(rows);
                    if (that.session)
                        that.session.end();
                });
        });
    }

    post(livro, callbackLivro) {
        const SessionFactory = require("../factory/sessionFactory");
        this.session = new SessionFactory();

        var that = this;
        this.session.connect(function () {

            that.session.connection.query("INSERT INTO Livro (lvTitulo, lvQuantidade, lvFoto, lvAno) values ('" + livro.lvTitulo + "'," + livro.lvQuantidade + ",'" + livro.lvFoto + "'," + livro.lvAno + ")",
                function (err, rows, fields) {
                    if (err)
                        throw err;
                    callbackLivro(rows);
                    if (that.session)
                        that.session.end();
                });
        });
    }

    update(livro, callbackLivro) {
        const SessionFactory = require("../factory/sessionFactory");
        this.session = new SessionFactory();

        var that = this;
        this.session.connect(function () {

            that.session.connection.query("UPDATE Livro SET lvTitulo = '" + livro.lvTitulo + "', lvQuantidade = " + livro.lvQuantidade + ", lvFoto = '" + livro.lvFoto + "', lvAno = " + livro.lvAno + " WHERE lvId = " + livro.lvId + "",
                function (err, rows, fields) {
                    if (err)
                        throw err;
                    callbackLivro(rows);
                    if (that.session)
                        that.session.end();
                });
        });
    }

    delete(livro, callbackLivro) {
        const SessionFactory = require("../factory/sessionFactory");
        this.session = new SessionFactory();

        var that = this;
        this.session.connect(function () {

            that.session.connection.query("DELETE FROM Livro WHERE lvId = " + livro.lvId + "",
                function (err, rows, fields) {
                    if (err)
                        throw err;
                    callbackLivro(rows);
                    if (that.session)
                        that.session.end();
                });
        });
    }
}
module.exports = LivroBusiness;