"use strict";

class LivroEmprestimoBusiness {

    findAll(emprestimo, callbackLivroEmprestimo) {
        const SessionFactory = require("../factory/sessionFactory");
        this.session = new SessionFactory();

        var that = this;
        this.session.connect(function () {

            that.session.connection.query("SELECT lvTitulo FROM LivroEmprestimo INNER JOIN Livro on LivroEmprestimo.lvId = Livro.lvId INNER JOIN Emprestimo on LivroEmprestimo.emId = LivroEmprestimo.emId WHERE LivroEmprestimo.emId =" + emprestimo.emId + "",
                function (err, rows, fields) {
                    if (err)
                        throw err;
                    callbackLivroEmprestimo(rows);
                    if (that.session)
                        that.session.end();
                });
        });
    }

    post(livroLivroEmprestimo, callbackLivroEmprestimo) {
        const SessionFactory = require("../factory/sessionFactory");
        this.session = new SessionFactory();

        var that = this;
        this.session.connect(function () {

            that.session.connection.query("INSERT INTO LivroEmprestimo (lvId, emId) values (" + livroLivroEmprestimo.lvId + "," + livroLivroEmprestimo.emId + ")",
                function (err, rows, fields) {
                    if (err)
                        throw err;
                    callbackLivroEmprestimo(rows);
                    if (that.session)
                        that.session.end();
                });
        });
    }

    update(livroLivroEmprestimo, callbackLivroEmprestimo) {
        const SessionFactory = require("../factory/sessionFactory");
        this.session = new SessionFactory();

        var that = this;
        this.session.connect(function () {

            that.session.connection.query("UPDATE LivroEmprestimo SET lvId = " + livroEmprestimo.lvId + ", emId = " + livroEmprestimo.emId + " WHERE lvId = " + livroEmprestimo.lvId + " AND emId = " + livroEmprestimo.emId +"",
                function (err, rows, fields) {
                    if (err)
                        throw err;
                    callbackLivroEmprestimo(rows);
                    if (that.session)
                        that.session.end();
                });
        });
    }

    delete(livroEmprestimo, callbackLivroEmprestimo) {
        const SessionFactory = require("../factory/sessionFactory");
        this.session = new SessionFactory();

        var that = this;
        this.session.connect(function () {

            that.session.connection.query("DELETE FROM LivroEmprestimo WHERE lvId = " + livroEmprestimo.lvId + " AND emId = " + livroEmprestimo.emId + "",
                function (err, rows, fields) {
                    if (err)
                        throw err;
                    callbackLivroEmprestimo(rows);
                    if (that.session)
                        that.session.end();
                });
        });
    }
}
module.exports = LivroEmprestimoBusiness;