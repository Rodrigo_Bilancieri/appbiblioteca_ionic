"use strict";

class UsuarioBusiness {

    findAll(usuario, callbackUsuario) {
        const SessionFactory = require("../factory/sessionFactory");
        this.session = new SessionFactory();

        var that = this;
        this.session.connect(function () {

            that.session.connection.query("SELECT * FROM Usuario WHERE usEmail='" + usuario.usEmail + "';",
                function (err, rows, fields) {
                    if (err)
                        throw err;
                    callbackUsuario(rows);
                    if (that.session)
                        that.session.end();
                });
        });
    }

    post(usuario, callbackUsuario) {
        const SessionFactory = require("../factory/sessionFactory");
        this.session = new SessionFactory();

        var that = this;
        this.session.connect(function () {

            that.session.connection.query("INSERT INTO Usuario (usNome, usEmail, usSenha) values ('" + usuario.usNome + "','" + usuario.usEmail + "','" + usuario.usSenha + "')",
                function (err, rows, fields) {
                    if (err)
                        throw err;
                    callbackUsuario(rows);
                    if (that.session)
                        that.session.end();
                });
        });
    }
}
module.exports = UsuarioBusiness;