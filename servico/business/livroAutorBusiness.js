"use strict";

class LivroAutorBusiness {

    /*findAll(callbackLivroAutor) {
        const SessionFactory = require("../factory/sessionFactory");
        this.session = new SessionFactory();

        var that = this;
        this.session.connect(function () {

            that.session.connection.query('SELECT * from LivroAutor',
                function (err, rows, fields) {
                    if (err)
                        throw err;
                    callbackLivroAutor(rows);
                    if (that.session)
                        that.session.end();
                });
        });
    }*/

    findAll(callbackLivroAutor) {
        const SessionFactory = require("../factory/sessionFactory");
        this.session = new SessionFactory();

        var that = this;
        this.session.connect(function () {

            that.session.connection.query("SELECT * FROM LivroAutor INNER JOIN Livro on LivroAutor.lvId = LivroAutor.lvId INNER JOIN Autor on LivroAutor.auId = Autor.auId;",
                function (err, rows, fields) {
                    if (err)
                        throw err;
                    callbackLivroAutor(rows);
                    if (that.session)
                        that.session.end();
                });
        });
    }

    post(livroAutor, callbackLivroAutor) {
        const SessionFactory = require("../factory/sessionFactory");
        this.session = new SessionFactory();

        var that = this;
        this.session.connect(function () {

            that.session.connection.query("INSERT INTO LivroAutor (lvId, auId) values (" + livroAutor.lvId + "," + livroAutor.auId + ")",
                function (err, rows, fields) {
                    if (err)
                        throw err;
                    callbackLivroAutor(rows);
                    if (that.session)
                        that.session.end();
                });
        });
    }

    update(livroAutor, callbackLivroAutor) {
        const SessionFactory = require("../factory/sessionFactory");
        this.session = new SessionFactory();

        var that = this;
        this.session.connect(function () {

            that.session.connection.query("UPDATE LivroAutor SET lvId = " + livroAutor.lvId + ", auId = " + livroAutor.auId + " WHERE lvId = " + livroAutor.lvId + " AND auId = " + livroAutor.auId + "",
                function (err, rows, fields) {
                    if (err)
                        throw err;
                    callbackLivroAutor(rows);
                    if (that.session)
                        that.session.end();
                });
        });
    }

    delete(livroAutor, callbackLivroAutor) {
        const SessionFactory = require("../factory/sessionFactory");
        this.session = new SessionFactory();

        var that = this;
        this.session.connect(function () {

            that.session.connection.query("DELETE FROM LivroAutor WHERE lvId = " + livroAutor.lvId + " AND auId = " + livroAutor.auId + "",
                function (err, rows, fields) {
                    if (err)
                        throw err;
                    callbackLivroAutor(rows);
                    if (that.session)
                        that.session.end();
                });
        });
    }
}
module.exports = LivroAutorBusiness;