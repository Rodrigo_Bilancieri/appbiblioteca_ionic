export interface ILivro {
    id: number;
    titulo: string;
    quantidade: number,
    ano: number;
    img: string;
    }