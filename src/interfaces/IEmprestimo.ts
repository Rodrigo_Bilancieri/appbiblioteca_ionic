export interface IEmprestimo {
    emDataInicio: string;
    emDataFim: string;
    emDataDevolucao: string;
    lvId: number;
    lvTitulo: string;
    lvFoto: string;
    lvAno: number;
    emId: number;
}