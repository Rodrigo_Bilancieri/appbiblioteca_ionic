import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular'; 
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { AnimatesDirective, AnimationService } from "css-animator";
import {Camera} from "@ionic-native/camera";
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { EmprestimosPage } from '../pages/emprestimos/emprestimos';
import { EmprestimoDetailsPage } from "../pages/emprestimo-details/emprestimo-details";
import { ListPage } from '../pages/list/list';
import { LivroListPage } from '../pages/livro-list/livro-list';
import { LivroListDeslogadoPage } from '../pages/livro-list-deslogado/livro-list-deslogado';
import { LivroProvider } from "../providers/livro";
import { LivroAddPage } from "../pages/livro-add/livro-add";
import { LivroDetailsPage } from "../pages/livro-details/livro-details";
import { AutorListPage } from '../pages/autor-list/autor-list';
import { AutorProvider } from "../providers/autor";
import { AutorAddPage } from "../pages/autor-add/autor-add";
import { AutorDetailsPage } from "../pages/autor-details/autor-details";
import { ChatPage } from "../pages/chat/chat";
import { SobrePage } from "../pages/sobre/sobre";

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { CategoriaProvider } from '../providers/categoria';
import { HttpModule } from "@angular/http";
import { HttpClientModule } from '@angular/common/http';
import { LoginProvider } from '../providers/login';
import { SigninPage } from '../pages/signin/signin';
import { SignupPage } from '../pages/signup/signup';
import { LivroAutorProvider } from '../providers/livro-autor';
import { AnimacoesPage } from '../pages/animacoes/animacoes';
import { SignupProvider } from '../providers/signup';
import { ConectaBancoProvider } from '../providers/conecta-banco';
import { EmprestaLivroProvider } from '../providers/empresta-livro';
import { UsuarioProvider } from '../providers/usuario';

// Colocamos o “export”, para podermos utilizar a mesma configuração em outras classes da nossa aplicação.
export const FIREBASECONFIG = {
  // Initialize Firebase
    apiKey: "AIzaSyD_37of72FO0sWzGol45YFQRnerNSQ31PM",
    authDomain: "projetobiblioteca-55564.firebaseapp.com",
    databaseURL: "https://projetobiblioteca-55564.firebaseio.com",
    projectId: "projetobiblioteca-55564",
    storageBucket: "projetobiblioteca-55564.appspot.com",
    messagingSenderId: "452621452022"
}


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    EmprestimosPage,
    EmprestimoDetailsPage,
    ListPage,
    LivroListPage,
    LivroListDeslogadoPage,
    LivroAddPage,
    LivroDetailsPage,
    AutorListPage,
    AutorAddPage,
    AutorDetailsPage,
    SigninPage,
    SignupPage,
    ChatPage,
    SobrePage,
    AnimacoesPage,
    AnimatesDirective
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    IonicModule.forRoot(MyApp),
    HttpModule,
    HttpClientModule,
    AngularFireModule.initializeApp(FIREBASECONFIG),
    AngularFireDatabaseModule,
    AngularFireAuthModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    EmprestimosPage,
    EmprestimoDetailsPage,
    ListPage,
    LivroListPage,
    LivroListDeslogadoPage,
    LivroAddPage,
    LivroDetailsPage,
    AutorListPage,
    AutorAddPage,
    AutorDetailsPage,
    SigninPage,
    SignupPage,
    ChatPage,
    SobrePage,
    AnimacoesPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    LivroProvider,
    LivroAddPage,
    LivroDetailsPage,
    AutorProvider,
    AutorAddPage,
    AutorDetailsPage,
    CategoriaProvider,
    LoginProvider,
    LivroAutorProvider,
    AnimationService,
    SignupProvider,
    ConectaBancoProvider,
    EmprestaLivroProvider,
    EmprestaLivroProvider,
    UsuarioProvider,
    Camera
  ]
})
export class AppModule { }
