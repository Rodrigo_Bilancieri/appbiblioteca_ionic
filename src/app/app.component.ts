import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import firebase from 'firebase';
import { FIREBASECONFIG } from './app.module';

import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { LivroListPage } from '../pages/livro-list/livro-list';
import { LivroListDeslogadoPage } from '../pages/livro-list-deslogado/livro-list-deslogado';
import { AutorListPage } from '../pages/autor-list/autor-list';
import { SigninPage } from "../pages/signin/signin";
import { LoginProvider } from "../providers/login";
import { ChatPage } from '../pages/chat/chat';
import { AnimacoesPage } from '../pages/animacoes/animacoes';
import { SobrePage } from '../pages/sobre/sobre';
import { SocialPage } from '../pages/social/social';
import { EmprestimosPage } from '../pages/emprestimos/emprestimos';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = HomePage;

  pages: Array<{ title: string, component: any }>;

  signinPage: any = SigninPage;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen, public loginProvider: LoginProvider) {

    firebase.initializeApp(FIREBASECONFIG);

    firebase.auth().onAuthStateChanged(user => {
      if (user) {
        this.getMenuLogado();
        this.nav.setRoot(this.rootPage);
      }
      else {
        this.getMenuAnonimo();
        this.nav.setRoot(this.signinPage);
      }
    });


    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Home', component: HomePage },
      // { title: 'List', component: ListPage },
      { title: 'Livros', component: LivroListPage },
      { title: 'Autores', component: AutorListPage }
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  // O evento openPage é executado toda vez que um novo item for acionado no menu. Assim,
  //estaremos executando o evento logout caso o usuário clicar em “Sair”.

  openPage(page) {
    if (page.title == "Sair") {
      this.loginProvider.logout();
      return;
    }

    this.nav.setRoot(page.component);
  }

  getMenuLogado() {
    this.pages = [
      { title: 'Home', component: HomePage },
      {title: 'Emprestimos', component: EmprestimosPage},
      //{ title: 'List', component: ListPage },
      { title: 'Livros', component: LivroListPage },
      { title: 'Autores', component: AutorListPage},
      { title: 'Chat', component: ChatPage},
      //{ title: 'Animacoes', component: AnimacoesPage },
      { title: 'Sobre', component: SobrePage },
      { title: 'Sair', component: null }
    ];
  }
  getMenuAnonimo() {
    this.pages = [
      { title: 'Home', component: HomePage },
      { title: 'Livros', component: LivroListDeslogadoPage },
      { title: 'Sobre', component: SobrePage },
      { title: 'Entrar', component: SigninPage },
    ];
  }

}
