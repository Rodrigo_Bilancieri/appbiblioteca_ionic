import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';

/**
 * Generated class for the ChatPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-chat',
  templateUrl: 'chat.html',
})
export class ChatPage {
  
  // A variável “observável” para as mensagens fica “atenta” quando existe qualquer alteração ou novidade nos dados.
  // As atualizações da variável são assíncronas, e a referência do serviço que ela enxergará é criada
  // com a função valueChanges().
  lista: Observable<any[]>;
  usuario: string;
  mensagem: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, public db: AngularFireDatabase) {
    this.lista = db.list('/chat').valueChanges();
    //console.log(this.lista);

  }

  // Inserção das mensagens no banco do Firebase.Isso é realizado no
  // método enviar(), onde será enviado para o Firebase as informações sobre o Usuario, a
  // Mensagem e a Hora Atual(que está no formato JSON por ser um tipo complexo)
  enviar() {
    let m = {
      user: this.usuario,
      texto: this.mensagem,
      data: new Date().toJSON()
    };

    let listItems = this.db.list('/chat');
    listItems.push(m).then(() => {
      this.mensagem = "";
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChatPage');
  }

}
