/* Estamos chamando o LoginProvider criado para autenticar o usuário, com as informações
inseridas no formulário. Usaremos o componente Loading Control para criar uma pequena
animação.
Se o usuário clicar no botão “Novo Usuário”, iremos trocar a navegação para SignUpPage. */

import { Component } from '@angular/core';
import { NgForm } from "@angular/forms";
import { LoadingController, AlertController, NavController, NavParams } from "ionic-angular";

import { LoginProvider } from "../../providers/login";
import { HomePage } from "../home/home";
import { SignupPage } from "../signup/signup";

@Component({
  selector: 'page-signin',
  templateUrl: 'signin.html'
})
export class SigninPage {

  backgrounds = [
    'assets/img/background/background1.jpg',
    'assets/img/background/background2.jpg',
    'assets/img/background/background3.jpg'
  ];

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    private loginProvider: LoginProvider,
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController) {
  }

  onSignin(form: NgForm) {
    const loading = this.loadingCtrl.create({
      content: 'Autenticando...'
    });

    loading.present();
    this.loginProvider.signin(form.value.email, form.value.password)
      .then(data => {
        loading.dismiss();
        this.navCtrl.push(HomePage, {});
      })
      .catch(error => {
        loading.dismiss();

        console.log(error);

        const alert = this.alertCtrl.create({
          title: 'Login falhou!',
          message: error.message,
          buttons: ['Ok']
        });
        alert.present();
      });
  }

  novoUsuario() {
    this.navCtrl.push(SignupPage, {});
  }
}
