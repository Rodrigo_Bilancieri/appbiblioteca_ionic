import { Component } from '@angular/core';
import { AlertController, IonicPage, NavController, NavParams } from 'ionic-angular';
import { LivroProvider } from "../../providers/livro";
import { UsuarioProvider } from "../../providers/usuario";
import { ILivro } from "../../interfaces/ILivro";
import { LivroAddPage } from "../livro-add/livro-add";
import { LivroDetailsPage } from "../livro-details/livro-details";
import { Http, Headers } from '@angular/http';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { EmprestaLivroProvider } from '../../providers/empresta-livro';
import firebase from "firebase";
import { IUsuario } from '../../interfaces/IUsuario';

@Component({
    selector: 'page-livro-list',
    templateUrl: 'livro-list.html'
})

export class LivroListPage {

    selectedItem: any;
    icons: string[];
    items: Array<ILivro>;
    pesquisa: string;
    visibilidade: boolean;
    itemsFilter: Array<ILivro>;
    usuarioLogado: Array<IUsuario>;
    emprestimo: Array<any>;

    novoItem(event, item) {
        this.navCtrl.push(LivroAddPage, {});
    }

    abrirPesquisa(event) {
        this.visibilidade = true;
    }

    pesquisar(event) {
        this.itemsFilter = this.items.filter((i) => {
            if (i.titulo.indexOf(this.pesquisa) >= 0) {
                return true;
            }
            return false;
        })
    }

    cancelarPesquisa() {
        this.visibilidade = false;
        this.pesquisa = "";
        this.pesquisar(null);
    }

    constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController, public livroProvider: LivroProvider, public usuarioProvider: UsuarioProvider, public emprestaLivroProvider: EmprestaLivroProvider, public http: HttpClient) {
        this.items = livroProvider.getLivros();
        this.itemsFilter = this.items;
        this.usuarioLogado = usuarioProvider.getUsuario();
        this.visibilidade = false;
    }

    itemTapped(/*event,*/ item) {
        console.log("item dentro da itemTapped():");
        console.log(item);
        // That's right, we're pushing to ourselves!
        this.navCtrl.push(LivroDetailsPage, {
            item: item
        });
    }

    emprestaLivro(item) {
        //this.usuarioProvider.getUsuario();
        var utc = new Date().toJSON().slice(0, 10).split("-");

        // Data para devolução
        var dataFim = "" + (parseInt(utc[2]) + 9) + "/" + utc[1] + "/" + utc[0];
        console.log("conteudo do items dentro da emprestaLivro()");
        console.log(item);
        let confirmar = this.alertCtrl.create({
            title: 'Confirmação',
            message: 'Deseja emprestar esse livro? <br> Data para devolução: ' + dataFim,
            buttons: [
                {
                    text: 'Cancelar',
                    handler: () => {
                        //console.log('Disagree clicked');
                    }
                },
                /*{
                    text: 'Emprestar',
                    handler: () => {
                        this.emprestaLivroProvider.criaEmprestimoNoBanco(item, this.usuarioLogado).then(data => {
                            this.emprestimo = data;
                            console.log("conteudo da emprestimo antes de relacionar:");
                            console.log(this.emprestimo);
                            this.emprestaLivroProvider.relacionaLivroEmprestimo(item, this.emprestimo);
                        });

                        this.emprestaLivroProvider.alteraQuantidadeLivro(item);
                    }
                }*/
                {
                    text: 'Emprestar',
                    handler: () => {
                        this.emprestaLivroProvider.criaEmprestimoNoBanco(item, this.usuarioLogado).then(data => {
                            this.emprestaLivroProvider.popularEmprestimoId().then(data => {
                                this.emprestimo = this.emprestaLivroProvider.getEmprestimos();
                                /*this.emprestimo = data;*/
                                console.log("conteudo da emprestimo antes de relacionar:");
                                console.log(this.emprestimo);
                                this.emprestaLivroProvider.relacionaLivroEmprestimo(item, this.emprestimo);
                                this.emprestaLivroProvider.alteraQuantidadeLivro(item);
                            })
                        });
                    }
                }
            ]
        });
        confirmar.present();


    }

    ionViewWillEnter() {
        this.items = [];
        this.usuarioLogado = [];
        this.usuarioProvider.popularUsuario();
        this.usuarioLogado = this.usuarioProvider.getUsuario();
        console.log("entrou na ionViewWillEnter da livro-list");
        this.livroProvider.popularLivros();
        this.items = this.livroProvider.getLivros();
        console.log(this.items);
        this.itemsFilter = this.items;
    }
}