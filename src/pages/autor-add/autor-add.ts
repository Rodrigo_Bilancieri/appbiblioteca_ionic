import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
//import { Camera, CameraOptions } from "@ionic-native/camera";
import { AutorProvider } from "../../providers/autor";
import { IAutor } from "../../interfaces/IAutor";

@IonicPage()
@Component({
  selector: 'page-autor-add',
  templateUrl: 'autor-add.html',
})
export class AutorAddPage {
  autor: IAutor;
  modoEdicao: boolean;
  imagemUploaded: boolean;

  constructor(public navCtrl: NavController, public navParams: NavParams, public autorProvider: AutorProvider/*, private camera: Camera*/) {
    this.autor = navParams.get("item");
    if (this.autor == null) {
      this.modoEdicao = false;
      this.autor = this.autorProvider.getInstancia();
    } else {
      this.modoEdicao = true;
    }
    this.imagemUploaded = (this.autor.img != "" && this.autor.img != null);
  }

  // Verificar como atualizar a tela com o novo autor cadastrado antes de fazer o pop
  salvar(evento) {
    if (!this.modoEdicao) {
      this.autorProvider.cadastraAutores(this.autor);
    } else {
      this.autorProvider.atualizaAutor(this.autor);
    }
    this.navCtrl.pop();
  }

  cancelar() {
    this.navCtrl.pop();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AutorAddPage');
  }

  // Metodos que utilizaram a camera

  /*tirarFoto() {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }
    this.camera.getPicture(options).then((imageData) => {
      let base64Image = 'data:image/jpeg;base64,' + imageData;
      this.autor.img = base64Image;
      this.imagemUploaded = true;
    }, (err) => {
    });
  }*/


  cancelarImagem() {
    this.autor.img = "";
    this.imagemUploaded = false;
  }

}
