import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EmprestimosPage } from './emprestimos';

@NgModule({
  declarations: [
    EmprestimosPage,
  ],
  imports: [
    IonicPageModule.forChild(EmprestimosPage),
  ],
})
export class EmprestimosPageModule {}
