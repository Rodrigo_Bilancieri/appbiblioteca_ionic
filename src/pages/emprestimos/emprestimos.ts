import { Component } from '@angular/core';
import { AlertController, IonicPage, NavController, NavParams } from 'ionic-angular';
import { UsuarioProvider } from "../../providers/usuario";
import { EmprestaLivroProvider } from '../../providers/empresta-livro';
import { IEmprestimo } from '../../interfaces/IEmprestimo';
import { IUsuario } from '../../interfaces/IUsuario';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { LivroProvider } from "../../providers/livro";
import { EmprestimoDetailsPage } from "../emprestimo-details/emprestimo-details";


@Component({
  selector: 'page-emprestimos',
  templateUrl: 'emprestimos.html',
})
export class EmprestimosPage {
  selectedItem: any;
  icons: string[];
  items: Array<IEmprestimo>;
  pesquisa: string;
  visibilidade: boolean;
  itemsFilter: Array<any>;
  usuarioLogado: Array<IUsuario>;
  emprestimo: Array<any>;

  abrirPesquisa(event) {
    this.visibilidade = true;
  }

  pesquisar(event) {
    this.itemsFilter = this.items.filter((i) => {
      if (i.lvTitulo.indexOf(this.pesquisa) >= 0) {
        return true;
      }
      return false;
    })
  }

  itemTapped(/*event,*/ item) {
    console.log("item dentro da itemTapped():");
    console.log(item);
    // That's right, we're pushing to ourselves!
    this.navCtrl.push(EmprestimoDetailsPage, {
      item: item
    });
  }

  removerEmprestimo() {

  }

  cancelarPesquisa() {
    this.visibilidade = false;
    this.pesquisa = "";
    this.pesquisar(null);
  }

  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController, public usuarioProvider: UsuarioProvider, public emprestaLivroProvider: EmprestaLivroProvider, public http: HttpClient) {
    this.usuarioLogado = usuarioProvider.getUsuario();
    this.items = emprestaLivroProvider.getEmprestimos();
    this.itemsFilter = this.items;
    this.visibilidade = false;
  }

  /*devolverLivro(items) {
    console.log("conteudo da items dentro da devolverLivros()");
    console.log(items);
    let alert = this.alertCtrl.create({
      title: 'Confirmação de devolução',
      message: 'Deseja devolver o Livro?',
      buttons: [
        {
          text: 'Não',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Sim',
          handler: () => {
            // Pega a data atual que será utilizada para informar a data de devolução
            var utc = new Date().toJSON().slice(0, 10);

            let livroDevolvido = {
              emDataInicio: items.emDataInicio,
              emDataFim: items.emDataFim,
              emDataDevolucao: utc,
              lvId: items.lvId,
              lvTitulo: items.lvTitulo,
              lvFoto: items.lvFoto,
              lvAno: items.lvAno,
              //lvQuantidade: items.lvQuantidade,
              emId: items.emId
            }
            console.log("Conteúdo da livroDevolvido na devolverLivro()");
            console.log(livroDevolvido);
            this.emprestaLivroProvider.atualizaEmprestimo(livroDevolvido);
          }
        }
      ]
    });
    alert.present();
  }*/

  ionViewWillEnter() {
    console.log("entrou na ionViewWillEnter() do emprestimo")
    this.items = [];
    console.log("entrou na ionViewWillEnter da livro-list");
    this.emprestaLivroProvider.popularEmprestimos();
    this.items = this.emprestaLivroProvider.getEmprestimos();
    console.log(this.items);
    this.itemsFilter = this.items;
  }

}