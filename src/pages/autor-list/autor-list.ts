import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { AutorProvider } from "../../providers/autor";
import { IAutor } from "../../interfaces/IAutor";
import { AutorAddPage } from "../autor-add/autor-add";
import { AutorDetailsPage } from "../autor-details/autor-details";
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
    selector: 'page-autor-list',
    templateUrl: 'autor-list.html'
})
export class AutorListPage {
    selectedItem: any;
    icons: string[];
    items: Array<IAutor>;
    pesquisa: string;
    visibilidade: boolean;
    itemsFilter: Array<IAutor>;
    
    /*async ionViewDidLoad() {
        console.log("Entrou na ionViewDidLoad: ")
        this.autorProvider.buscaAutoresDoBanco();
    }*/


    novoItem(event, item) {
        this.navCtrl.push(AutorAddPage, {});
    }

    abrirPesquisa(event) {
        this.visibilidade = true;
    }

    pesquisar(event) {
        this.itemsFilter = this.items.filter((i) => {
            if (i.nome.indexOf(this.pesquisa) >= 0) {
                return true;
            }
            return false;
        })
    }

    cancelarPesquisa() {
        this.visibilidade = false;
        this.pesquisa = "";
        this.pesquisar(null);
    }

    constructor(public navCtrl: NavController, public navParams: NavParams, public autorProvider: AutorProvider, public http: HttpClient) {
        this.items = autorProvider.getAutores();
        this.itemsFilter = this.items;
        this.visibilidade = false;
    }

    itemTapped(event, item) {
        // That's right, we're pushing to ourselves!
        this.navCtrl.push(AutorDetailsPage, {
            item: item
        });
    }

    ionViewWillEnter() {
        this.items = [];
        console.log("entrou na ionViewWillEnter da livro-list");
        this.autorProvider.popularAutor();
        this.items = this.autorProvider.getAutores();
        console.log(this.items);
        this.itemsFilter = this.items;
    }
}