import { Component } from '@angular/core';
import { AlertController, IonicPage, NavController, NavParams } from 'ionic-angular';
import { IEmprestimo } from '../../interfaces/IEmprestimo';
import { EmprestaLivroProvider } from "../../providers/empresta-livro";

@IonicPage()
@Component({
  selector: 'page-emprestimo-details',
  templateUrl: 'emprestimo-details.html',
})
export class EmprestimoDetailsPage {
  emprestimo: IEmprestimo;

  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController, public emprestaLivroProvider: EmprestaLivroProvider) {
    this.emprestimo = navParams.get('item');
  }

  removerItem(event) {
    let confirmar = this.alertCtrl.create({
      title: 'Confirmação',
      message: 'Deseja excluir esse registro?',
      buttons: [
        {
          text: 'Cancelar',
          handler: () => {
            //console.log('Disagree clicked');
          }
        },
        {
          text: 'Excluir',
          handler: () => {
            //this.emprestaLivroProvider.(this.livro);
            this.navCtrl.pop();
          }
        }
      ]
    });
    confirmar.present();
  }

  devolverLivro(emprestimo) {
    console.log("conteudo da items dentro da devolverLivros()");
    console.log(this.emprestimo);
    let alert = this.alertCtrl.create({
      title: 'Confirmação de devolução',
      message: 'Deseja devolver este livro?',
      buttons: [
        {
          text: 'Não',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Sim',
          handler: () => {
            // Pega a data atual que será utilizada para informar a data de devolução
            var utc = new Date().toJSON().slice(0, 10);

            let livroDevolvido = {
              emDataInicio: null,
              emDataFim: this.emprestimo.emDataFim,
              emDataDevolucao: utc,
              lvId: this.emprestimo.lvId,
              lvTitulo: this.emprestimo.lvTitulo,
              lvFoto: this.emprestimo.lvFoto,
              lvAno: this.emprestimo.lvAno,
              //lvQuantidade: items.lvQuantidade,
              emId: this.emprestimo.emId
            }
            console.log("Conteúdo da livroDevolvido na devolverLivro()");
            console.log(livroDevolvido);
            this.emprestaLivroProvider.atualizaEmprestimo(livroDevolvido);
            this.navCtrl.pop();
          }
        }
      ]
    });
    alert.present();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EmprestimoDetailsPage');
    console.log(this.emprestimo);
  }

}
