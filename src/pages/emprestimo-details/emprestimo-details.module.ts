import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EmprestimoDetailsPage } from './emprestimo-details';

@NgModule({
  declarations: [
    EmprestimoDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(EmprestimoDetailsPage),
  ],
})
export class EmprestimoDetailsPageModule {}
