import { Component } from '@angular/core';
import { AlertController, IonicPage, NavController, NavParams } from 'ionic-angular';
import { Camera, CameraOptions } from "@ionic-native/camera";
import { AutorProvider } from "../../providers/autor";
import { LivroAutorProvider } from "../../providers/livro-autor";
import { LivroProvider } from "../../providers/livro";
import { ILivro } from "../../interfaces/ILivro";

@IonicPage()
@Component({
  selector: 'page-livro-add',
  templateUrl: 'livro-add.html',
})
export class LivroAddPage {
  livro: ILivro;
  modoEdicao: boolean;
  imagemUploaded: boolean;

  constructor(public navCtrl: NavController, public navParams: NavParams, public livroProvider: LivroProvider, private camera: Camera, public alertCtrl: AlertController, public autorProvider: AutorProvider,
    public livroAutorProvider: LivroAutorProvider) {

    this.livro = navParams.get("item");

    if (this.livro == null) {
      this.modoEdicao = false;
      this.livro = this.livroProvider.getInstancia();
    }
    else
      this.modoEdicao = true;

    this.imagemUploaded = (this.livro.img != "" && this.livro.img != null);
  }

  salvar(evento) {
    if (!this.modoEdicao)
      this.livroProvider.cadastraLivros(this.livro);
    else
      this.livroProvider.atualizaLivro(this.livro);
    this.navCtrl.pop();
  }

  cancelar() {
    this.navCtrl.pop();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LivroAddPage');
  }

  // Metodos que utilizaram a camera

  /*Para cancelar a imagem, o atributo img da variável livro será setado para vazio (“”). Em
  seguida, setamos a variável imagemUploaded para falso, com intuito de alterar a visibilidade
  dos componentes da tela.*/
  cancelarImagem() {
    this.livro.img = "";
    this.imagemUploaded = false;
  }

  /*Esse método foi retirado da documentação oficial do Ionic
  Dentro do método passamos a imagem retirada da foto para a propriedade img da variável
  livro. Além disso setamos o atributo imagemUploaded para true.*/
  tirarFoto() {
    const options: CameraOptions = {
      targetWidth: 300,
      targetHeight: 300,
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
    }
    this.camera.getPicture(options).then((imageData) => {
      let base64Image = 'data:image/jpeg;base64,' + imageData;
      this.livro.img = base64Image;
      this.imagemUploaded = true;
    }, (err) => {
    });
  }

  /*O método escolherImagem() é praticamente igual ao método anterior, com excessão de
  algumas propriedades a mais na variável interna options.
  A propriedade mais importante porém é a: sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
  pois é com ela que é possível setar o driver da câmera para pegar uma imagem originada da
  Galeria.*/
  escolherImagem() {
    const options: CameraOptions = {
      targetWidth: 300,
      targetHeight: 300,
      quality: 50,
      destinationType: this.camera.DestinationType.DATA_URL,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      allowEdit: true,
      saveToPhotoAlbum: false
    }
    this.camera.getPicture(options).then((imageData) => {
      let base64Image = 'data:image/jpeg;base64,' + imageData;
      this.livro.img = base64Image;
      this.imagemUploaded = true;
    }, (err) => {
    });
  }



  /* Quando o usuário clicar no botão “Gerenciar”, esse
  evento será chamado. A ideia aqui é consultar primeiro todos os autores presentes na tabela
  de Autor e, após, todos os Autores cadastrados para o Livro que está sendo incluído/alterado.
  Após, iremos criar um “Alert” com as opções de autores para o usuário selecionar. A ideia é
  que sejam listados TODOS os autores do cadastro de Autor. Porém, APENAS os autores já
  relacionados aos livros devem aparecer com o checkbox selecionado.
  De forma parecida com as funções filter() e findIndex() que utilizamos anteriormente, existe
  uma função chamada some(). Essa função retorna verdadeiro caso pelo menos um elemento
  do array satisfaça determinada condição. A condição do nosso caso é que o autor autor do laço
  de repetição esteja na listagem dos Autores já cadastrados. Armazenamos se o autor está ou
  não na lista na variável possuiAutor.
  Após essa verificação, podemos inserir um novo item (addInput) na listagem de checkbox com
  as informações do autor e se ele já está ou não relacionado ao livro. Caso estiver, o atributo
  checked será true. Senão, false.
  Ao final inserimos dois botões: Cancelar e Salvar. O cancelar apenas fecha o AlertController. O
  Salvar chama a função adicionarLivroAutor do LivroAutorProvider, passando o livro atual do
  cadastro e um array contendo apenas os items (autores) que foram checados pelo usuário.
  Esse array é do tipo Array<string>, por isso criamos o parâmetro do método com esse tipo.
  O método alert.present() serve para apresentar (show) o alert criado no cadastro.*/
  gerenciarAutores() {
    let autores = this.autorProvider.getAutores();
    let livrosAutores =
      this.livroAutorProvider.getLivrosAutores(this.livro);
    let alert = this.alertCtrl.create();
    alert.setTitle('Selecione os Autores:');
    for (let i = 0; i < autores.length; i++) {
      //livrosAutores
      let possuiAutor: boolean = livrosAutores.some((a) => {
        if (a.autorId == autores[i].id) {
          return true;
        }
        return false;
      });
      alert.addInput({
        type: 'checkbox',
        label: autores[i].nome,
        value: autores[i].id.toString(),
        checked: possuiAutor
      });
    }
    alert.addButton('Cancelar');
    alert.addButton({
      text: 'Salvar',
      handler: data => {
        this.livroAutorProvider.adicionarLivroAutor(this.livro, data);
      }
    });
    alert.present();
  }


}
