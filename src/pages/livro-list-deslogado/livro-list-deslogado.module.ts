import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LivroListDeslogadoPage } from './livro-list-deslogado';

@NgModule({
  declarations: [
    LivroListDeslogadoPage,
  ],
  imports: [
    IonicPageModule.forChild(LivroListDeslogadoPage),
  ],
})
export class LivroListDeslogadoPageModule {}
