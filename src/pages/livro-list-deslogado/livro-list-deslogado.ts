import { Component } from '@angular/core';
import { AlertController, IonicPage, NavController, NavParams } from 'ionic-angular';
import { LivroProvider } from "../../providers/livro";
import { UsuarioProvider } from "../../providers/usuario";
import { ILivro } from "../../interfaces/ILivro";
import { LivroAddPage } from "../livro-add/livro-add";
import { LivroDetailsPage } from "../livro-details/livro-details";
import { Http, Headers } from '@angular/http';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import firebase from "firebase";
import { IUsuario } from '../../interfaces/IUsuario';

@Component({
  selector: 'page-livro-list-deslogado',
  templateUrl: 'livro-list-deslogado.html',
})

export class LivroListDeslogadoPage {

  selectedItem: any;
  icons: string[];
  items: Array<ILivro>;
  pesquisa: string;
  visibilidade: boolean;
  itemsFilter: Array<ILivro>;
  //usuarioLogado: Array<IUsuario>;
  emprestimo: Array<any>;

  novoItem(event, item) {
    this.navCtrl.push(LivroAddPage, {});
  }

  abrirPesquisa(event) {
    this.visibilidade = true;
  }

  pesquisar(event) {
    this.itemsFilter = this.items.filter((i) => {
      if (i.titulo.indexOf(this.pesquisa) >= 0) {
        return true;
      }
      return false;
    })
  }

  cancelarPesquisa() {
    this.visibilidade = false;
    this.pesquisa = "";
    this.pesquisar(null);
  }

  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController, public livroProvider: LivroProvider, public http: HttpClient) {
    this.items = livroProvider.getLivros();
    this.itemsFilter = this.items;
    this.visibilidade = false;
  }

  itemTapped(/*event,*/ item) {
    console.log("item dentro da itemTapped():");
    console.log(item);
    // That's right, we're pushing to ourselves!
    this.navCtrl.push(LivroDetailsPage, {
      item: item
    });
  }

  ionViewWillEnter() {
    this.items = this.livroProvider.getLivros();
    this.itemsFilter = this.items;
  }
}