import { HttpClient } from '@angular/common/http';
import { Http, Headers } from '@angular/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the ConectaBancoProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ConectaBancoProvider {

  constructor(public http: HttpClient) {
    console.log('Hello ConectaBancoProvider Provider');
  }

  buscaDoBanco(API_URL) {
    var xhr = new XMLHttpRequest();

    xhr.open("GET", API_URL);

    xhr.addEventListener("load", function () {
      var resposta = xhr.responseText;
      var entidades = JSON.parse(resposta);
      console.log("Entidade: ");
      console.log(entidades);
      entidades.forEach(function (entidade) {

      });

    });

    xhr.send();
  }

}
