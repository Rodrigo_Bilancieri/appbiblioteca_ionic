import { Injectable } from '@angular/core';
import { IAutor } from "../interfaces/IAutor";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/map';

@Injectable()
export class AutorProvider {
    public API_URL = 'https://fast-mountain-12659.herokuapp.com/api/v1/unip/biblioteca/autor';
    private autor: IAutor[] = [];
    
    constructor(public http: HttpClient) {
        if (this.autor.length == 0)
            this.popularAutor();
            //this.buscaAutoresDoBanco();
    }
    
    cadastraAutores(autorParam) {

        let headers = new HttpHeaders();
        headers.append('Accept', 'application/json');
        headers.append('Content-Type', 'application/json');

        let autor = {
            auNome: autorParam.nome,
            auIdade: autorParam.idade,
            auFoto: autorParam.foto
        }

        return new Promise<Array<any>>((resolve, reject) => {
            this.http.post(this.API_URL, autor, { headers: headers })
                .subscribe((data: Array<any>) => {
                    resolve()
                }, err => {
                    reject(err);
                });
        });
    }

    buscaAutoresDoBanco(): Promise<Array<any>> {
        let headers = new HttpHeaders();
        headers.append('Accept', 'application/json');
        headers.append('Content-Type', 'application/json');

        return new Promise<Array<any>>((resolve, reject) => {
            this.http.get(this.API_URL, { headers: headers })
                .subscribe((data: Array<any>) => {
                    resolve(data)
                }, err => {
                    reject(err);
                });
        });
    }

    async popularAutor() {
        this.autor = [];
        const autores = await this.buscaAutoresDoBanco();
        for(let autor of autores) {
            this.autor.push({ id: autor.auId, nome: autor.auNome, idade: autor.auIdade, img: autor.auFoto });
        }        
    }

    getAutores(): IAutor[] {
        return this.autor;
    }

    excluirAutor(autorParam: IAutor) {
        let autor = {
            auId: autorParam.id
        }

        console.log(autor);

        let headers = new HttpHeaders();
        headers.append('Accept', 'application/json');
        headers.append('Content-Type', 'application/json');

        this.API_URL = this.API_URL + '/' + autor.auId

        console.log(this.API_URL);

        return new Promise<Array<any>>((resolve, reject) => {
            this.http.delete(this.API_URL)
                .subscribe((data: Array<any>) => {
                    resolve()
                }, err => {
                    reject(err);
                });
        });
    }

    /*removerAutor(autor: IAutor) {
        let position = this.autor.findIndex((a: IAutor) => {
            return a.id == autor.id;
        })
        this.autor.splice(position, 1);
    }*/

    atualizaAutor(autorParam: IAutor) {

        let headers = new HttpHeaders();
        headers.append('Accept', 'application/json');
        headers.append('Content-Type', 'application/json');
        
        let autor = {
            auId: autorParam.id,
            auNome: autorParam.nome,
            auIdade: autorParam.idade,
            auFoto: autorParam.img
        }

        return new Promise<Array<any>>((resolve, reject) => {
            this.http.put(this.API_URL, autor, { headers: headers })
                .subscribe((data: Array<any>) => {
                    resolve()
                }, err => {
                    reject(err);
                });
        });

}

    /*alterarAutor(autor: IAutor) {
        console.log(this.autor);
        let position = this.autor.findIndex((a: IAutor) => {
            return a.id == autor.id;
        })
        this.autor[position].nome = autor.nome;
        this.autor[position].idade = autor.idade;
        this.autor[position].img = autor.img;
    }*/

    getInstancia(): IAutor {
        return {
            id: 0,
            nome: "",
            idade: null,
            img: ""
        };
    }

    private getNextID(): number {
        let nextId = 0;
        if (this.autor.length > 0) {
            nextId = Math.max.apply(
                Math, this.autor.map(function (o) { return o.id; })
            );
        }
        return ++nextId;
    }
}