import { Injectable } from '@angular/core';
import { ILivro } from "../interfaces/ILivro";
import { Http, Headers } from '@angular/http';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/map';

@Injectable()
export class LivroProvider {
    public API_URL = 'https://fast-mountain-12659.herokuapp.com/api/v1/unip/biblioteca/livro';
    private livros: ILivro[] = [];

    constructor(public http: HttpClient) {
        if (this.livros.length == 0)
            this.popularLivros();
    }

    cadastraLivros(livroParam) {
        console.log("livroParam:");
        console.log(livroParam);

        let headers = new HttpHeaders();
        headers.append('Accept', 'application/json');
        headers.append('Content-Type', 'application/json');

        let livro = {
            lvTitulo: livroParam.titulo,
            lvQuantidade: 1,
            lvFoto: livroParam.img,
            lvAno: livroParam.ano
        }

        console.log(livro);

        return new Promise<Array<any>>((resolve, reject) => {
            this.http.post(this.API_URL, livro, { headers: headers })
                .subscribe((data: Array<any>) => {
                    resolve()
                }, err => {
                    reject(err);
                });
        });
    }

    buscaLivrosDoBanco(): Promise<Array<any>> {
        let headers = new HttpHeaders;
        headers.append('Accept', 'application/json');
        headers.append('Content-Type', 'application/json');

        return new Promise<Array<any>>((resolve, reject) => {
            this.http.get(this.API_URL, { headers: headers })
                .subscribe((data: Array<any>) => {
                    resolve(data)
                }, err => {
                    reject(err);
                });
        });
    }

    atualizaLivro(livroParam: ILivro) {
        let headers = new HttpHeaders();
        headers.append('Accept', 'application/json');
        headers.append('Content-Type', 'application/json');

        let livro = {
            lvId: livroParam.id,
            lvTitulo: livroParam.titulo,
            lvAno: livroParam.ano,
            lvFoto: livroParam.img,
            lvQuantidade: livroParam.quantidade
        }
        console.log("Conteudo da livro dentro da atualizaLivro:");
        console.log(livro);

        return new Promise<Array<any>>((resolve, reject) => {
            this.http.put(this.API_URL, livro, { headers: headers })
                .subscribe((data: Array<any>) => {
                    resolve()
                }, err => {
                    reject(err);
                });
        });
    }

    excluirLivro(livroParam: ILivro) {
        let livro = {
            lvId: livroParam.id
        }

        console.log(livro);

        let headers = new HttpHeaders();
        headers.append('Accept', 'application/json');
        headers.append('Content-Type', 'application/json');

        this.API_URL = this.API_URL + '/' + livro.lvId

        console.log(this.API_URL);

        return new Promise<Array<any>>((resolve, reject) => {
            this.http.delete(this.API_URL)
                .subscribe((data: Array<any>) => {
                    resolve()
                }, err => {
                    reject(err);
                });
        });
    }

    async popularLivros() {
        this.livros = [];
        let livrosParam = await this.buscaLivrosDoBanco();
        for (let livros of livrosParam) {
            this.livros.push({ id: livros.lvId, titulo: livros.lvTitulo, ano: livros.lvAno, quantidade: livros.lvQuantidade, img: livros.lvFoto });
        }
    }

    getLivros(): ILivro[] {
        return this.livros;
    }

    adicionarLivro(livro: ILivro) {
        if (livro.id == 0)
            livro.id = this.getNextID();
        this.livros.push(livro);
    }
    removerLivro(livro: ILivro) {
        let position = this.livros.findIndex((l: ILivro) => {
            return l.id == livro.id;
        })
        this.livros.splice(position, 1);
    }
    alterarLivro(livro: ILivro) {
        let position = this.livros.findIndex((l: ILivro) => {
            return l.id == livro.id;
        })
        this.livros[position].titulo = livro.titulo;
        this.livros[position].ano = livro.ano;
        this.livros[position].img = livro.img;
    }

    getInstancia(): ILivro {
        return {
            id: 0,
            titulo: "",
            quantidade: 0,
            ano: null,
            img: ""
        };
    }

    private getNextID(): number {
        let nextId = 0;
        if (this.livros.length > 0) {
            nextId = Math.max.apply(
                Math, this.livros.map(function (o) { return o.id; })
            );
        }
        return ++nextId;
    }
}