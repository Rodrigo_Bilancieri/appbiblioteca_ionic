import { Injectable } from '@angular/core';
import firebase from 'firebase';

@Injectable()
export class LoginProvider {

  // Cria um novo usuário na base do firebase
  signup(email: string, password: string) {
    return firebase.auth().createUserWithEmailAndPassword(email,
      password);
  }

  // Loga o usuário na aplicação
  signin(email: string, password: string) {
    return firebase.auth().signInWithEmailAndPassword(email,
      password);
  }
  
  // Desconecta o usuário
  logout() {
    firebase.auth().signOut();
  }
}
