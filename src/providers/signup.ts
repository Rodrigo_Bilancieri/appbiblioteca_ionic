/*import { HttpClient } from '@angular/common/http';*/
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/map';

@Injectable()
export class SignupProvider {
  API_URL = 'https://fast-mountain-12659.herokuapp.com/api/v1/unip/biblioteca/usuario';

  constructor(public http: HttpClient) {
    console.log('Hello SignupProvider Provider');
  }

  cadastraUsuario(usuarioParam) {
    console.log("Conteudo da usuarioParam");
    console.log(usuarioParam);

    let headers = new HttpHeaders();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json');

    let usuario = {
      usNome: usuarioParam.nome,
      usEmail: usuarioParam.email,
      usSenha: usuarioParam.password
    }

    console.log("Conteudo da usuario:");
    console.log(usuario);

    return new Promise<Array<any>>((resolve, reject) => {
      this.http.post(this.API_URL, usuario, { headers: headers })
        .subscribe((data: Array<any>) => {
          resolve()
        }, err => {
          reject(err);
        });
    });
  }

}
