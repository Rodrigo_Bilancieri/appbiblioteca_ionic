import { Injectable } from '@angular/core';
import { ILivroAutor } from "../interfaces/ILivroAutor";
import { ILivro } from "../interfaces/ILivro";

@Injectable()
export class LivroAutorProvider {
  private livroAutores: ILivroAutor[] = [];
  constructor() {
  }

  /* No método getInstancia, da mesma forma dos providers que criamos anteriormente, esse
  método retornará uma instância vazia(sem nenhum valor setado para os atributos) de um
  objeto que implemente a interface correspondente ILivroAutor. */
  getInstancia(): ILivroAutor {
    return {
      livroId: null,
      autorId: null
    };
  }

  /* No método getLivrosAutores, iremos pesquisar APENAS os registros da “tabela” LivroAutor
  cujo livro for o mesmo que estiver sido passado por parâmetro.*/
  getLivrosAutores(livro: ILivro): ILivroAutor[] {
    let itemsFilter = this.livroAutores.filter((i) => {
      if (i.livroId == livro.id) {
        return true;
      }
      return false;
    });
    return itemsFilter;
  }

  /* No método adicionarLivroAutor, os parâmetros passados serão o Livro do cadastro e uma lista
  de Autores. A explicação do objeto autores ser do tipo Array<string> será explicado melhor
  posteriormente.
  Nesse método, a primeira coisa a ser feita é excluir todos os autores antigos do cadastro
  daquele livro. Assim executamos a função removerLivroAutor. Em seguida, para cada item do
  array autores nós iremos incluir um novo objeto LivroAutor, setando as propriedades
  necessárias para o cadastro.*/
  adicionarLivroAutor(livro: ILivro, autores: Array<string>) {
    this.removerLivroAutor(livro);
    for (let x = 0; x < autores.length; x++) {
      let novoItem = this.getInstancia();
      novoItem.livroId = livro.id;
      novoItem.autorId = parseInt(autores[x]);
      this.livroAutores.push(novoItem);
    }
  }

/* No método removerLivroAutor, iremos remover todos os autores do livro que estiver sido
passado por parâmetro. No caso dessa implementação, que não utiliza o banco de dados,
basta apenas filtra todos os registros que tiverem o livro diferente do parâmetro e aplicar
esses registros no array da classe provider.*/
  removerLivroAutor(livro: ILivro) {
    let itemsFilter = this.livroAutores.filter((i) => {
      if (i.livroId != livro.id) {
        return true;
      }
      return false;
    });
    this.livroAutores = itemsFilter;
  }
}