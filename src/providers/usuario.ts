import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import firebase from "firebase";
import 'rxjs/add/operator/map';
import { IUsuario } from '../interfaces/IUsuario';

@Injectable()
export class UsuarioProvider {
  public API_URL = 'https://fast-mountain-12659.herokuapp.com/api/v1/unip/biblioteca/usuario';
  public usuario: IUsuario[] = [];

  constructor(public http: HttpClient) {
    console.log('Hello UsuarioProvider Provider');
    if (this.usuario.length == 0) {
      this.popularUsuario();
    }
  }

  buscaUsuarioNoBanco() {

    var emailUsuarioLogado = firebase.auth().currentUser.email;

    let usuario = {
      usEmail: emailUsuarioLogado
    }

    let headers = new HttpHeaders();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json');
    headers.append("Accept", "Access - Control - Allow - Origin");

    let API_URL_USAVEL = "";
    API_URL_USAVEL = this.API_URL + '/' + usuario.usEmail;

    return new Promise<Array<any>>((resolve, reject) => {
      this.http.get(API_URL_USAVEL, { headers: headers })
        .subscribe((data: Array<any>) => {
          resolve(data)
        }, err => {
          reject(err);
        });
    });
  }

  async popularUsuario() {

    this.usuario = [];

    let usuarioId = await this.buscaUsuarioNoBanco();
    for(let usuario of usuarioId) {
      this.usuario.push({usId: usuario.usId});
    }
  }

  getUsuario(): any {
    return this.usuario;
  }

}
