import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class CategoriaProvider {

  apiUrl = 'http://localhost:3014/api/v1';
  //private categorias = null;

  constructor(public http: Http) {
    //console.log('Hello CategoriaProvider Provider');
  }

  getCategorias() {

    /*if (this.categorias) {
      return Promise.resolve(this.categorias);
    }*/

    return new Promise(resolve => {
      this.http.get(this.apiUrl + '/categorias')
        .map(res => res.json())
        .subscribe(dados => {
          //this.categorias = dados;
          //resolve(this.categorias);
          resolve(dados);
        });
    });
  }
}
