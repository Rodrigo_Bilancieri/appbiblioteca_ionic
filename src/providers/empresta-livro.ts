import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ILivro } from "../interfaces/ILivro";
import 'rxjs/add/operator/map';
import firebase from "firebase";
import { forEach } from '@firebase/util';
import { IEmprestimo } from '../interfaces/IEmprestimo';
import { IUsuario } from '../interfaces/IUsuario';
import { UsuarioProvider } from "../providers/usuario";
import { AlertController, IonicPage } from 'ionic-angular';

@Injectable()
export class EmprestaLivroProvider {

  public API_URL = 'https://fast-mountain-12659.herokuapp.com/api/v1/unip/biblioteca/emprestimo';
  private iEmprestimo: IEmprestimo[] = [];
  //usuarioLogado: Array<IUsuario>;
  usuarioLogado: IUsuario[] = [];


  constructor(public http: HttpClient, public usuarioProvider: UsuarioProvider, public alertCtrl: AlertController) {
    if (this.iEmprestimo.length == 0) {
      console.log("entrou")
      //this.usuarioLogado = usuarioProvider.getUsuario();
      this.popularEmprestimos();
    }

  }

  defineData() {

    var data = new Date();
    var dataCorreta = [];

    var dia = data.getDate();
    var mes = data.getMonth();
    var ano = data.getFullYear();
    mes = mes + 1;

    dataCorreta.push(ano, mes, dia);

    return dataCorreta;
  }

  criaEmprestimoNoBanco(livroParam, usuarioParam) {

    let headers = new HttpHeaders();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json');

    let emprestimo = {
      lvId: livroParam.id,
      usId: usuarioParam[0].usId,
      emDataDevolucao: null
    }

    console.log("emprestimo sendo criado: ");
    console.log(emprestimo);

    return new Promise<Array<any>>((resolve, reject) => {
      this.http.post(this.API_URL, emprestimo, { headers: headers })
        .subscribe((data: Array<any>) => {
          resolve(data)
        }, err => {
          if (err.status == 400) {
            let alert = this.alertCtrl.create({
              title: 'Erro',
              subTitle: err.error.message,
              buttons: ['Ok']
            });
            alert.present();
          } else {
            reject(err);
          }
        });
    });

  }

  buscaEmprestimosNoBanco(usuarioParam) {
    console.log("entrou na buscaEmprestimoNoBanco(usuarioParam)");

    console.log(usuarioParam);

    let usuario = {
      usId: usuarioParam
    }

    console.log("conteúdo da usuario")
    console.log(usuario);
    console.log("fim do conteúdo da usuario")

    let headers = new HttpHeaders();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json');
    headers.append("Accept", "Access - Control - Allow - Origin");

    let API_URL_USAVEL = "";
    API_URL_USAVEL = this.API_URL + '/' + usuario.usId;

    console.log("conteúdo da usuario dentro da buscaEmprestimoNoBanco:")
    console.log(usuario)

    return new Promise<Array<any>>((resolve, reject) => {
      this.http.get(API_URL_USAVEL, { headers: headers })
        .subscribe((data: Array<any>) => {
          resolve(data)
        }, err => {
          reject(/*err*/);
        });
    });
  }

  async popularEmprestimos() {

    this.iEmprestimo = [];

    console.log("conteudo da usuarioParam na popularEmprestimos");
    await this.usuarioProvider.buscaUsuarioNoBanco().then(data => {
      this.usuarioLogado = data[0].usId;
      // console.log(data[0].usId)
      //console.log(this.usuarioLogado);
      //console.log("fim")
    })
    await console.log("oi!")
    await console.log(this.usuarioLogado);

    let emprestimos = await this.buscaEmprestimosNoBanco(this.usuarioLogado);

    for (let emprestimo of emprestimos) {

      let emDataFim = emprestimo.emDataFim.split("-");
      emDataFim = emprestimo.emDataFim.split("T")[0];
      emDataFim = emDataFim.split("-");
      let diaF = emDataFim[2];
      let mesF = emDataFim[1];
      let anoF = emDataFim[0];
      emDataFim = diaF + '/' + mesF + '/' + anoF;

      let emDataInicio = emprestimo.emDataInicio.split("-");
      emDataInicio = emprestimo.emDataInicio.split("T")[0];
      emDataInicio = emDataInicio.split("-");
      let diaI = emDataInicio[2];
      let mesI = emDataInicio[1];
      let anoI = emDataInicio[0];
      emDataInicio = diaI + '/' + mesI + '/' + anoI;

      this.iEmprestimo.push({ emId: emprestimo.emId, emDataInicio, emDataFim, emDataDevolucao: emprestimo.emDataDevolucao, lvTitulo: emprestimo.lvTitulo, lvId: emprestimo.lvId, lvFoto: emprestimo.lvFoto, lvAno: emprestimo.lvAno });
    }
  }

  // Busca apenas o último id de empréstimo do usuário no banco
  buscaEmprestimoId(usuarioParam) {

    //Limpa o array antes de começar
    //this.iEmprestimo = [];

    console.log("entrou na buscaEmprestimoId(usuarioParam)");
    console.log(usuarioParam);

    let API_URL = 'https://fast-mountain-12659.herokuapp.com/api/v1/unip/biblioteca/buscaid';

    let usuario = {
      usId: usuarioParam
    }

    console.log("conteúdo da usuario")
    console.log(usuario);
    console.log("fim do conteúdo da usuario")

    let headers = new HttpHeaders();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json');
    headers.append("Accept", "Access - Control - Allow - Origin");

    let API_URL_USAVEL = "";
    API_URL_USAVEL = API_URL + '/' + usuario.usId;

    console.log("conteúdo da usuario dentro da buscaEmprestimoId:")
    console.log(usuario)

    return new Promise<Array<any>>((resolve, reject) => {
      this.http.get(API_URL_USAVEL, { headers: headers })
        .subscribe((data: Array<any>) => {
          resolve(data)
        }, err => {
          reject(/*err*/);
        });
    });
  }

  // Vai popular o array de interface de Emprestimo apenas com o último ID de empréstimo do usuário
  async popularEmprestimoId() {

    // Limpa o Array antes de começar
    this.iEmprestimo = [];

    // Busca o usuário no banco para executar a query que vai buscar o empréstimo
    await this.usuarioProvider.buscaUsuarioNoBanco().then(data => {
      this.usuarioLogado = data[0].usId;
    })
    await console.log("oi!")
    await console.log(this.usuarioLogado);

    const emprestimos = await this.buscaEmprestimoId(this.usuarioLogado);

    for (let emprestimo of emprestimos) {

      let emDataFim = emprestimo.emDataFim.split("-");
      emDataFim = emprestimo.emDataFim.split("T")[0];
      emDataFim = emDataFim.split("-");
      let dia = emDataFim[2];
      let mes = emDataFim[1];
      let ano = emDataFim[0];
      emDataFim = dia + '/' + mes + '/' + ano;

      this.iEmprestimo.push({ emId: emprestimo.emId, emDataInicio: emprestimo.emDataInicio, emDataFim, emDataDevolucao: emprestimo.emDataDevolucao, lvTitulo: emprestimo.lvTitulo, lvId: emprestimo.lvId, lvFoto: emprestimo.lvFoto, lvAno: emprestimo.lvAno });
    }
  }

  getEmprestimos(): IEmprestimo[] {
    return this.iEmprestimo;
  }

  relacionaLivroEmprestimo(livroParam, emprestimoParam) {
    let API_URL = 'https://fast-mountain-12659.herokuapp.com/api/v1/unip/biblioteca/livroemprestimo';

    console.log("conteúdo da emprestimoParam dentro da relacionaLivroEmprestimo");
    console.log(emprestimoParam);

    let headers = new HttpHeaders();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json');

    let dado = {
      emId: emprestimoParam[0].emId,
      lvId: livroParam.id,
    }

    console.log("relacionando livro e emprestimo:");
    console.log(dado.emId, dado.lvId);

    return new Promise<Array<any>>((resolve, reject) => {
      this.http.post(API_URL, dado, { headers: headers })
        .subscribe((data: Array<any>) => {
          resolve()
        }, err => {
          reject(err);
        });
    });

  }

  alteraQuantidadeLivro(livroParam) {
    let API_URL = 'https://fast-mountain-12659.herokuapp.com/api/v1/unip/biblioteca/livro';

    let headers = new HttpHeaders();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json');

    let livro = {
      lvId: livroParam.id,
      lvTitulo: livroParam.titulo,
      lvQuantidade: livroParam.quantidade = livroParam.quantidade - 1,
      lvFoto: livroParam.img,
      lvAno: livroParam.ano
    }

    console.log("conteudo da livro dentro da alteraQuantidadeLivro:");
    console.log(livro);

    return new Promise<Array<any>>((resolve, reject) => {
      this.http.put(API_URL, livro, { headers: headers })
        .subscribe((data: Array<any>) => {
          resolve()
        }, err => {
          reject(err);
        });
    });
  }

  buscaLivrosEmprestados(emprestimo) {
    console.log("buscaLivrosEmprestados:");
    console.log(emprestimo);

    let headers = new HttpHeaders();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json');
    headers.append("Accept", "Access - Control - Allow - Origin");

    let API_URL = "https://fast-mountain-12659.herokuapp.com/api/v1/unip/biblioteca/livroemprestimo"
    let API_URL_USAVEL = "";
    API_URL_USAVEL = API_URL + '/' + emprestimo;

    return new Promise<Array<any>>((resolve, reject) => {
      this.http.get(API_URL_USAVEL, { headers: headers })
        .subscribe((data: Array<any>) => {
          resolve(data)
        }, err => {
          reject(err);
        });
    });
  }

  atualizaEmprestimo(livroDevolvido: IEmprestimo) {
    let headers = new HttpHeaders();
    headers.append('Accept', 'application/json');
    headers.append('Content-Type', 'application/json');

    console.log("Conteudo do livroDevolvido dentro da atualizaEmprestimo:");
    console.log(livroDevolvido);

    return new Promise<Array<any>>((resolve, reject) => {
      this.http.put(this.API_URL, livroDevolvido, { headers: headers })
        .subscribe((data: Array<any>) => {
          resolve()
        }, err => {
          reject(err);
        });
    });
  }

}
